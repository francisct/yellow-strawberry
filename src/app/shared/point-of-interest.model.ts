import { LatLng } from "leaflet";

export class PointOfInterest {
    img: string
    label: string
    latlng: LatLng
    metadata: any

    constructor(img: string,
        label: string,
        latlng: LatLng,
        metadata: any) {
        this.img = img;
        this.label = label
        this.latlng = latlng
        this.metadata = metadata
    }
}