import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
    {state: 'starter', name: 'Home', type: 'link', icon: 'av_timer' },
    {state: 'button', type: 'link', name: 'Map', icon: 'crop_7_5'},   
]; 

@Injectable()

export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }

}
