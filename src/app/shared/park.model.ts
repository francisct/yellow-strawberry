import { LatLng } from "@yaga/leaflet-ng2";

export class Park {
    name: string
    latlng: LatLng
    score: number
    filtersRequested: Array<string> = new Array()
    metadata: any;

    constructor(name: string,
        latlng: LatLng,
        score: number,
        metadata: any) {
        this.name = name;
        this.latlng = latlng;
        this.score = score;
        this.metadata = metadata;
    }

    addScore(score: number) {
        this.score += score;
    }
}