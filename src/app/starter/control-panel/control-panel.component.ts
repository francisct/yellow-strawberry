import { MatBottomSheetRef } from "@angular/material/bottom-sheet";
import { Component } from "@angular/core";
import { ParkService } from "../../shared/park.service";
import { Filter } from "../../shared/filter.model";

@Component({
  selector: 'control-panel',
  templateUrl: 'control-panel.component.html',
  styleUrls: ['control-panel.component.scss']
})
export class ControlPanelComponent {

  constructor(public parkService: ParkService, private bottomSheetRef: MatBottomSheetRef<ControlPanelComponent>) {

  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }


  onFilterClick(filter: Filter) {
    const exist = this.parkService.selectedFilters.indexOf(filter)
    if (exist == -1)
      this.parkService.selectedFilters.unshift(filter)
  }

  onFilterRemoveClick(filter: Filter) {
    this.parkService.selectedFilters = this.parkService.selectedFilters.filter(el => el.label != filter.label);
  }

  onFindPark() {
    this.parkService.findPark();
    this.bottomSheetRef.dismiss();
  }



}