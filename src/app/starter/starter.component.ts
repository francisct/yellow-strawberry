import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormControl, Validators } from '@angular/forms';
import { OSM_TILE_LAYER_URL, MapComponent, LatLngExpression, LatLng, LeafletMouseEvent } from '@yaga/leaflet-ng2';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { ParkService } from '../shared/park.service';
import { Park } from '../shared/park.model';
import { MatDialog } from '@angular/material';
import { ParkDetailsComponent } from './park-details/park-details.component';
import { PointOfInterest } from '../shared/point-of-interest.model';

@Component({
  selector: 'starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.scss']
})
export class StarterComponent implements AfterViewInit {
  public tileLayerUrl: string = OSM_TILE_LAYER_URL;
  private centerOfQuebec: LatLngExpression = new LatLng(45.521743896993634, -73.61938476562501)


  @ViewChild(MapComponent) map: MapComponent
  constructor(public parkService: ParkService, private bottomSheet: MatBottomSheet, public dialog: MatDialog) {

  }

  ngAfterViewInit() {
    this.map.flyTo(this.centerOfQuebec, 10);
  }

  onMapMove() {
    this.map.invalidateSize();
    this.map.on('click', ($event) => this.onMapClick($event));
  }
  onMapClick($event) {
    if (this.parkService.bestParks.length == 0) {
      const latlng: LatLng = $event.latlng
      if (!this.parkService.buddyLocations.find(point => point.lat == latlng.lat && point.lng == latlng.lng))
        this.parkService.buddyLocations.push(latlng);
    }
  }


  openBottomSheet(): void {
    const instance = this.bottomSheet.open(ControlPanelComponent);
    instance.afterDismissed().subscribe(el => this.map.flyTo(this.centerOfQuebec, 11))
  }

  getMarkerColor(aPark: Park): any {
    let MeanScore = 0;
    let TotalScore = 0;

    this.parkService.bestParks.forEach(el => {
      TotalScore += el.score;
    })
    MeanScore = TotalScore / this.parkService.bestParks.length;
    if (aPark.score > MeanScore) {
      return 'green';
    }
    else {
      return 'yellow';
    }
  }

  openParkDetails(park: Park) {
    let dialogRef = this.dialog.open(ParkDetailsComponent, {
      width: '90vw',
      data: park
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.zoomPark(park)
      else {
        this.parkService.interests = new Array()
      }
    })
  }

  back() {
    this.parkService.interests = new Array();
    this.map.flyTo(this.centerOfQuebec, 11)
  }

  clear() {
    this.parkService.buddyLocations = new Array();
    this.parkService.bestParks = new Array();
    this.parkService.selectedFilters = new Array();
    this.parkService.interests = new Array();
  }

  zoomPark(park: Park) {
    this.map.flyTo(park.latlng, 16);
  }
}
