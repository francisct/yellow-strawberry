import { Component, AfterViewInit, Inject, Output, EventEmitter } from "@angular/core";
import { Park } from "../../shared/park.model";
import { DialogOverviewExampleDialog } from "../../material-component/dialog/dialog.component";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ParkService } from "../../shared/park.service";

@Component({
    selector: 'park-details',
    templateUrl: './park-details.component.html',
    styleUrls: ['./park-details.component.scss']
})
export class ParkDetailsComponent implements AfterViewInit {

    icons: Array<string> = new Array();
    @Output()
    ngAfterViewInit(): void {
    }

    constructor(public dialogRef: MatDialogRef<ParkDetailsComponent>, @Inject(MAT_DIALOG_DATA) public park: Park, private parkService: ParkService) {
        this.parkService.findPointOfInterests(this.park);
        const salut = this.parkService.interests.map(interest => interest.img)
        this.icons = salut.filter(function (item, pos) {
            return salut.indexOf(item) == pos;
        })
    }

    displayPOIs() {
        this.dialogRef.close(true);
    }

}